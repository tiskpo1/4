

<%@page contentType="text/html" pageEncoding="UTF-8"%>

        <div id="main">
            <aside class="leftAside">
                <h2>Районна библиотека</h2>
                <ul>
                    <li><a href="#">Классическая литература</a></li>
                    <li><a href="#">Современная литература</a></li>
                    <li><a href="#">Приключения</a></li>
                    <li><a href="#">Фантастика</a></li>
                    
                </ul>
            </aside>
            ${param.name}
            <section>
                <article>
                    <h1>${article.title}</h1>
                    <div class="text-article">
                        ${article.text}
                    </div>
                    <div class="fotter-article">
                        <span class="date-article">Дата: ${article.date}</span>
                    </div>
                </article>
            </section>
        </div>


