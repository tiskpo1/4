

<%@page contentType="text/html" pageEncoding="UTF-8"%>

        <div id="main">
            <aside class="leftAside">
                <h2>Районна библиотека</h2>
                <ul>
                    <li><a href="#">Классическая литература</a></li>
                    <li><a href="#">Современная литература</a></li>
                    <li><a href="#">Приключения</a></li>
                    <li><a href="#">Фантастика</a></li>
                    
                </ul>
            </aside>
            <section>   
                    <c:forEach var="article" items="${articles}">
                        <article>
			<h1>${article.title}</h1>
                        <div class="text-article">
                        ${fn:substring(article.text,0,300)} ...
                        </div>
                        <div class="fotter-article">
                        <span class="read"><a href="article?id=${article.id}">Читать...</a></span>
                        <span class="date-article">Дата: ${article.date}</span>
                        </div>
                        </article>
		    </c:forEach>
            </section>
        </div>
        
